# Assignment3_CSharp_Web_API_creation_ASP.NET_Core
This project creates a Movie API which can be manipulated with Swagger

## Installation

To install and run this project, follow these steps:

1. Install the .NET SDK on your machine if you haven't already.

2. Clone this repository to your local machine using the following command:
```
 git clone https://gitlab.com/JNatamna/assignment3_csharp_web_api_creation_asp.net_core
```
3. Navigate to the project directory using the following command
```
cd your-repo.
```
4. Open the project.
Change the connection string in appsetting.json to your own.
```
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft.AspNetCore": "Warning"
    }
  },
    "AllowedHosts": "*",
  "ConnectionStrings": {
    "default": "Your own connection string\\SQLEXPRESS; Initial Catalog = MovieEF; Integrated Security = True; Trust Server Certificate = True"
  }
}
```
5. Build the project using the following commands:
```
dotnet restore
dotnet build
update-database
```
## Usage

This project is focused on creating databases and manipulating them with Swagger. It can be run in Visual Studio 2022 with .NET6. To run the program, follow these steps:

    1. Open the project solution file in Visual Studio.

    2. Run the project by pressing F5


## Contributers

Simen Heiestad Mandt\
Joachim Noor Atamna\
Kristian Boberg

## License
MIT 2023



