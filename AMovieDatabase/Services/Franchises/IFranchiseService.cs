﻿using AMovieDatabase.Models.Entities;

namespace AMovieDatabase.Services.Franchises
{
    public interface IFranchiseService : ICrudService<Franchise, int>
    {
        /// <summary>
        /// Returns a collection of movies associated with the specified franchise ID.
        /// </summary>
        /// <param name="franchiseId">The ID of the franchise to retrieve movies for.</param>
        /// <returns>A task representing the asynchronous operation. The task result contains a collection of movies.</returns>
        Task<ICollection<Movie>> GetMoviesInFranchiseAsync(int franchiseId);
        /// <summary>
        /// Returns a collection of characters associated with the specified franchise ID.
        /// </summary>
        /// <param name="franchiseId">The ID of the franchise to retrieve characters for.</param>
        /// <returns>A task representing the asynchronous operation. The task result contains a collection of characters.</returns>
        Task<ICollection<Character>> GetCharactersInFranchiseAsync(int franchiseId);

        /// <summary>
        /// Updates the specified movies' franchise ID to the new franchise ID.
        /// </summary>
        /// <param name="movieIds">An array of integers representing the IDs of the movies to update.</param>
        /// <param name="franchiseId">An integer representing the ID of the new franchise to associate the movies with.</param>
        /// <returns>A task representing the asynchronous operation.</returns>
        Task UpdateMoviesAsync(int[] movieIds, int franchiseId);
    }
}
