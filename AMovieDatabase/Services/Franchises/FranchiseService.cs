﻿using AMovieDatabase.Models;
using AMovieDatabase.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace AMovieDatabase.Services.Franchises
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieDbContext _context;

        public FranchiseService(MovieDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Franchise>> GetAllAsync()
        {
            return await _context.Franchises
                .Include(c => c.Movies)
                .ToListAsync();
        }

        public async Task<Franchise> GetByIdAsync(int id)
        {
            return await _context.Franchises
                .Where(f => f.Id == id)
                .Include(m => m.Movies)
                .FirstAsync();
        }

        public async Task AddAsync(Franchise obj)
        {
            await _context.Franchises.AddAsync(obj);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                // Exception
                Console.WriteLine("Franchise not found with id: " + id);
            }
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public async Task<Franchise> UpdateAsync(Franchise obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return obj;
        }

        private async Task<bool> FranchiseExistsAsync(int id)
        {
            return await _context.Franchises.AnyAsync(c => c.Id == id);
        }
   
        public async Task UpdateMoviesAsync(int[] movieIds, int franchiseId)
        {
            if (!await FranchiseExistsAsync(franchiseId))
            {
                Console.WriteLine("Franchise not found with Id: " + franchiseId);
            }
            List<Movie> movies = movieIds
                .ToList()
                .Select(sid => _context.Movies
                .Where(m => m.Id == sid).First())
                .ToList();
            Franchise franchise = await _context.Franchises
                .Where(f => f.Id == franchiseId)
                .FirstAsync();
            franchise.Movies = movies;
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Character>> GetCharactersInFranchiseAsync(int idFranchise)
        {
            return await _context.Movies
            .Where(movie => movie.FranchiseId == idFranchise)
            .Include(movie => movie.Characters)
            .SelectMany(movie => movie.Characters)
            .Distinct()
            .ToListAsync();
        }

        public async Task<ICollection<Movie>> GetMoviesInFranchiseAsync(int franchiseId)
        {
            return await _context.Movies
            .Where(m => m.FranchiseId == franchiseId)
            .Include(m => m.Characters)
            .ToListAsync();
        }
    }
}
