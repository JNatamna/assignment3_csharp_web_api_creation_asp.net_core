﻿using AMovieDatabase.Models;
using AMovieDatabase.Models.Entities;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;

namespace AMovieDatabase.Services.Movies
{
    public class MovieService : IMovieService
    {
        private readonly MovieDbContext _context;


        public MovieService(MovieDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Adds a Movie object to the database, asynchronously.
        /// </summary>
        /// <param name="obj">The Movie object to add to the database.</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        public async Task AddAsync(Movie obj)
        {
            await _context.Movies.AddAsync(obj);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Returns a collection of all movies in the database, including their associated franchises and characters, asynchronously.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation. The task result contains the collection of movies.</returns>
        public async Task<IEnumerable<Movie>> GetAllAsync()
        {
            return await _context.Movies
                .Include(m => m.Franchise)
                .Include(c => c.Characters)
                .ToListAsync();
        }

        /// <summary>
        /// Returns a movie from the database, including its associated franchise and characters, based on its ID, asynchronously.
        /// </summary>
        /// <param name="id">The ID of the movie to retrieve from the database.</param>
        /// <returns>A task that represents the asynchronous operation. The task result contains the movie.</returns>
        public async Task<Movie> GetByIdAsync(int id)
        {
            return await _context.Movies
                .Where(m => m.Id == id)
                .Include(f => f.Franchise)
                .Include(c => c.Characters)
                .FirstAsync();
        }

        /// <summary>
        /// Returns a collection of characters associated with a specific movie, based on its ID, asynchronously.
        /// </summary>
        /// <param name="id">The ID of the movie to retrieve characters for.</param>
        /// <returns>A task that represents the asynchronous operation. The task result contains the collection of characters.</returns>
        public async Task<IEnumerable<Character>> GetCharacters(int id)
        {
            return (await _context.Movies
                .Where(m => m.Id == id)
                .Include(c => c.Characters)
                .FirstAsync()).Characters;
        }

        /// <summary>
        /// Updates a movie in the database, asynchronously.
        /// </summary>
        /// <param name="obj">The movie to update in the database.</param>
        /// <returns>A task that represents the asynchronous operation. The task result contains the updated movie.</returns>
        public async Task<Movie> UpdateAsync(Movie obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return obj;
        }

        /// <summary>
        /// Deletes a movie from the database based on its ID, asynchronously.
        /// </summary>
        /// <param name="id">The ID of the movie to delete from the database.</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        public async Task DeleteByIdAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Retrieves all characters associated with a given movie
        /// </summary>
        /// <param name="id">The id of the movie to retrieve characters for</param>
        /// <returns>A collection of characters associated with the specified movie</returns>
        public async Task<ICollection<Character>> GetCharactersAsync(int id)
        {
            Movie movie = await _context.Movies.Where(m => m.Id == id).Include(c => c.Characters).FirstAsync();

            return movie.Characters.ToList();
        }

        /// <summary>
        /// Updates the characters associated with a given movie
        /// </summary>
        /// <param name="idMovie">The id of the movie to update characters for</param>
        /// <param name="characterIds">An array of character ids to associate with the movie</param>
        /// <returns>An awaitable task</returns>
        public async Task UpdateCharactersAsync(int idMovie, int[] characterIds)
        {
            Movie movie = await _context.Movies
            .Where(m => m.Id == idMovie)
            .Include(m => m.Characters)
            .FirstAsync();

            List<Character> characters = new List<Character>();

            foreach (int id in characterIds)
            {
                Character character = await _context.Characters
                .Where(c => c.Id == id)
                .FirstAsync();
                characters.Add(character);
            }

            movie.Characters = characters;
            _context.Entry(movie).State = EntityState.Modified;

            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates the franchise associated with a given movie
        /// </summary>
        /// <param name="idMovie">The id of the movie to update the franchise for</param>
        /// <param name="idFranchise">The id of the franchise to associate with the movie</param>
        /// <returns>An awaitable task</returns>
        public async Task UpdateFranchiseAsync(int idMovie, int idFranchise)
        {
            Movie movie = await _context.Movies
            .Where(m => m.Id == idMovie).FirstAsync();

            Franchise franchise = await _context.Franchises
            .Where(f => f.Id == idFranchise)
            .FirstAsync();

            movie.Franchise = franchise;
            _context.Entry(movie).State = EntityState.Modified;

            await _context.SaveChangesAsync();
        }
    }
}
