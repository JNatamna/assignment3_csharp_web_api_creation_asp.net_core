﻿using AMovieDatabase.Models.Entities;

namespace AMovieDatabase.Services.Movies
{
    public interface IMovieService : ICrudService<Movie, int>
    {
        /// <summary>
        /// Retrieve all characters associated with a movie from the database
        /// </summary>
        /// <param name="id">The id of the movie to retrieve characters for</param>
        /// <returns>A collection of all characters associated with the movie</returns>
        Task<ICollection<Character>> GetCharactersAsync(int id);

        /// <summary>
        /// Updates the list of characters associated with a movie in the database
        /// </summary>
        /// <param name="idMovie">The id of the movie to update</param>
        /// <param name="characterIds">An array of character ids to associate with the movie</param>
        /// <returns>A Task representing the asynchronous operation</returns>
        Task UpdateCharactersAsync(int idMovie, int[] characterIds);

        /// <summary>
        /// Updates the franchise associated with a movie in the database
        /// </summary>
        /// <param name="idMovie">The id of the movie to update</param>
        /// <param name="idFranchise">The id of the franchise to associate with the movie</param>
        /// <returns>A Task representing the asynchronous operation</returns>
        Task UpdateFranchiseAsync(int idMovie, int idFranchise);
    }
}
