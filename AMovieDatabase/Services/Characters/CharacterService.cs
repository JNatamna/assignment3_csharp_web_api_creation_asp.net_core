﻿using AMovieDatabase.Models;
using AMovieDatabase.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace AMovieDatabase.Services.Characters
{
    public class CharacterService : ICharacterService
    {
        private readonly MovieDbContext _context;

        public CharacterService(MovieDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Adds a new character to the database.
        /// </summary>
        /// <param name="obj">The character object to add.</param>
        public async Task AddAsync(Character obj)
        {
            await _context.Characters.AddAsync(obj);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes a character from the database by its ID.
        /// </summary>
        /// <param name="id">The ID of the character to delete.</param>
        public async Task DeleteByIdAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null) 
            {
                // Exception
                Console.WriteLine("Character not found with id: " + id);
            }
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Returns a list of all characters with their movies from the database.
        /// </summary>
        /// <returns>A collection of all characters with their movies.</returns>
        public async Task<IEnumerable<Character>> GetAllAsync()
        {
            return await _context.Characters
                .Include(c => c.Movies)
                .ToListAsync();
        }

        /// <summary>
        /// Returns a character by its ID with its movies from the database.
        /// </summary>
        /// <param name="id">The ID of the character to retrieve.</param>
        /// <returns>The character object with its movies.</returns>
        public async Task<Character> GetByIdAsync(int id)
        {
            return await _context.Characters
                .Where(c => c.Id == id)
                .Include(m => m.Movies)
                .FirstAsync();
        }

        /// <summary>
        /// Returns the movies of a character by its ID from the database.
        /// </summary>
        /// <param name="id">The ID of the character to retrieve the movies for.</param>
        /// <returns>A collection of movies the character appears in.</returns>
        public async Task<IEnumerable<Movie>> GetMovies(int id)
        {
            return (await _context.Characters
                .Where(c => c.Id == id)
                .Include(m => m.Movies)
                .FirstAsync()).Movies;
        }

        /// <summary>
        /// Updates a character in the database.
        /// </summary>
        /// <param name="obj">The character object to update.</param>
        /// <returns>The updated character object.</returns>
        public async Task<Character> UpdateAsync(Character obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return obj;
        }

        private async Task<bool> CharacterExistsAsync(int id)
        {
            return await _context.Characters.AnyAsync(c => c.Id == id);
        }
    }
}
