﻿using AMovieDatabase.Models.Entities;

namespace AMovieDatabase.Services.Characters
{
    public interface ICharacterService : ICrudService<Character, int>
    {
        /// <summary>
        /// Retrieves a collection of movies associated with the given id, along with their associated data.
        /// </summary>
        /// <param name="id">The id used to retrieve the movies.</param>
        /// <returns>A collection of movies and their associated data.</returns>
        Task<IEnumerable<Movie>> GetMovies(int id);
    }
}
