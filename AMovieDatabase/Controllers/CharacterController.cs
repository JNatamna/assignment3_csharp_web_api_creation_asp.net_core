﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AMovieDatabase.Services.Characters;
using AutoMapper;
using AMovieDatabase.Models.DTO.Characters;
using AMovieDatabase.Models.Entities;

namespace AMovieDatabase.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class CharacterController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;
        
        public CharacterController(ICharacterService characterService, IMapper mapper)
        {
            _mapper = mapper;
            _characterService = characterService;
        }

        /// <summary>
        /// Retrieve all characters stored in the database, along with their associated data
        /// </summary>
        /// <returns>A collection of all characters and their associated data</returns>
        // GET: api/Characters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOCharacter>>> GetCharactersAsync()
        {
            return Ok(_mapper.Map<List<DTOCharacter>>
                (
                    await _characterService.GetAllAsync()
                )
            );
        }

        /// <summary>
        /// Retrieve a character from the database based on the given id
        /// </summary>
        /// <returns>A character object representing the requested character</returns>
        /// <param name="id">The id of the character to retrieve</param>
        // GET: api/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOCharacter>> GetCharacterAsync(int id)
        {
            var character = _mapper.Map<DTOCharacter>(await _characterService.GetByIdAsync(id));

            if (character == null)
            {
                return NotFound();
            }

            return character;
        }

        /// <summary>
        /// Updates a character in the database based on the given id
        /// </summary>
        /// <returns>An IActionResult indicating the status of the operation</returns>
        /// <param name="id">The id of the character to update</param>
        /// <param name="dto">The DTO containing the updated character data</param>
        // PUT: api/Characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacterAsync(int id, DTOCharacterPUT dto)
        {
            if (id != dto.Id)
            {
                return NotFound();
            }

            try
            {
                await _characterService.UpdateAsync(_mapper.Map<Character>(dto));
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new character to the database
        /// </summary>
        /// <returns>An ActionResult containing the newly created character</returns>
        /// <param name="dto">The DTO containing the data for the new character</param>
        // POST: api/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult> PostCharacterAsync(DTOCharacterPOST dto)
        {
            Character character = _mapper.Map<Character>(dto);
            await _characterService.AddAsync(character);
            return CreatedAtAction("GetCharacterAsync", new { id = character.Id }, character);
        }

        /// <summary>
        /// Removes a character from the database based on given id
        /// </summary>
        /// <returns>An IActionResult indicating the status of the operation</returns>
        /// <param name="id">The id of the character to delete</param>
        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacterAsync(int id)
        {
            var character = await _characterService.GetByIdAsync(id);
            if(character == null)
            {
                return NotFound();
            }

            try
            {
                await _characterService.DeleteByIdAsync(id);
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
