﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AMovieDatabase.Models;
using AMovieDatabase.Models.Entities;
using AMovieDatabase.Services.Movies;
using AutoMapper;
using AMovieDatabase.Models.DTO.Movie;
using System.Net;
using AMovieDatabase.Models.DTO.Characters;

namespace AMovieDatabase.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public MoviesController(IMovieService movieService, IMapper mapper)
        {
            _mapper = mapper;
            _movieService = movieService;
        }


        /// <summary>
        /// Retrieve all movies in the database along with their associated data represented as IDs
        /// </summary>
        ///  <returns>A collection of all movies and their associated data</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOMovie>>> GetMovies()
        {
            return Ok(_mapper.Map<List<DTOMovie>>
                (
                    await _movieService.GetAllAsync()
                )
            );
        }

        /// <summary>
        /// Retrieves a specific movie from the database based on its movie ID.
        /// </summary>
        /// <param name="id">The ID of the movie to retrieve</param>
        /// <returns>A character object representing the requested movie</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOMovie>> GetMovie(int id)
        {
            var movie = _mapper.Map<DTOMovie>(await _movieService.GetByIdAsync(id));

            if (movie == null)
            {
                return NotFound();
            }

            return movie;
        }

        /// <summary>
        /// Updates a movie in the database based on its movie ID
        /// </summary>
        /// <param name="id">The ID of the movie to update</param>
        /// <param name="dto">A DTO representing the updated movie information</param>
        ///<returns>An IActionResult indicating the status of the operation</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, DTOMoviePUT dto)
        {
            if (id != dto.Id)
            {
                return NotFound();
            }

            try
            {
                await _movieService.UpdateAsync(_mapper.Map<Movie>(dto));
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a new movie in the database
        /// </summary>
        /// <param name="dto">A DTO representing the new movie information</param>
        /// <returns>An ActionResult containing the newly created movie</returns>
        // POST: api/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult> PostMovie(DTOMoviePOST dto)
        {
            Movie movie = _mapper.Map<Movie>(dto);
            await _movieService.AddAsync(movie);
            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }

        /// <summary>
        /// Deletes a movie from the database based on the MovieId 
        /// </summary>
        /// <param name="id">The ID of the movie to retrieve characters for</param>
        /// <returns>An IActionResult indicating the status of the operation</returns>

        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<DTOCharacterINFO>>> GetCharactersInMovieAsync(int id)
        {
            try
            {
                return Ok(_mapper.Map<List<DTOCharacterINFO>>
                    (await _movieService.GetCharactersAsync(id)));
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Updates the characters in a movie.
        /// </summary>
        /// <param name="id">The ID of the movie.</param>
        /// <param name="characterIds">An array of character IDs to update.</param>
        /// <returns>Returns NoContent if the update is successful. Returns NotFound if the movie is not found.</returns>
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateCharactersInMovie(int id, int[] characterIds)
        {
            try
            {
                await _movieService.UpdateCharactersAsync(id, characterIds);
                return NoContent();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Updates the franchise of a movie.
        /// </summary>
        /// <param name="id">The ID of the movie.</param>
        /// <param name="franchiseId">The ID of the franchise to update.</param>
        /// <returns>Returns NoContent if the update is successful. Returns NotFound if the movie is not found.</returns>
        [HttpPut("{id}/franchise")]
        public async Task<IActionResult> UpdateFranchiseInMovie(int id, int franchiseId)
        {
            try
            {
                await _movieService.UpdateFranchiseAsync(id, franchiseId);
                return NoContent();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Deletes a movie by ID.
        /// </summary>
        /// <param name="id">The ID of the movie to delete.</param>
        /// <returns>Returns NoContent if the delete is successful. Returns NotFound if the movie is not found.</returns>
        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _movieService.GetByIdAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            try
            {
                await _movieService.DeleteByIdAsync(id);
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
