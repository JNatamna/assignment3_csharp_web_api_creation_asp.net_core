﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AMovieDatabase.Services.Franchises;
using AutoMapper;
using AMovieDatabase.Models.DTO.Franchises;
using System.Net;
using AMovieDatabase.Models.DTO.Characters;
using AMovieDatabase.Models.DTO.Movie;
using AMovieDatabase.Models.Entities;

namespace AMovieDatabase.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchiseController : ControllerBase
    {
        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;

        public FranchiseController(IFranchiseService franchiseService, IMapper mapper)
        {
            _mapper = mapper;
            _franchiseService = franchiseService;
        }

        /// <summary>
        /// Retrieve all franchises stored in the database, along with their associated data
        /// </summary>
        /// <returns>A collection of all franchises and their associated data</returns>
        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOFranchise>>> GetFranchisesAsync()
        {
            return Ok(_mapper.Map<List<DTOFranchise>>
                (
                    await _franchiseService.GetAllAsync()
                )
            );
        }

        /// <summary>
        /// Retrieve a franchise from the database based on the given id
        /// </summary>
        /// <param name="id">The ID of the franchise to retrieve.</param>
        /// <returns>A franchise object representing the requested franchise</returns>
        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOFranchise>> GetFranchiseAsync(int id)
        {
            var franchise = _mapper.Map<DTOFranchise>(await _franchiseService.GetByIdAsync(id));

            if (franchise == null)
            {
                return NotFound();
            }

            return franchise;
        }

        /// <summary>
        /// Updates a franchise in the database based on the given id
        /// </summary>
        /// <param name="id">The ID of the franchise to update.</param>
        /// <param name="dto">An object representing the franchise data to update.</param>
        /// <returns>A IActionResult indicating the status of the operation</returns>
        // PUT: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchiseAsync(int id, DTOFranchisePUT dto)
        {
            if (id != dto.Id)
            {
                return NotFound();
            }
            try
            {
                await _franchiseService.UpdateAsync(_mapper.Map<Franchise>(dto));
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new franchise to the database
        /// </summary>
        /// <param name="dto">An object representing the franchise data to add.</param>
        /// <returns>An ActionResult containing the newly created franchise</returns>
        // POST: api/Franchises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult> PostFranchiseAsync(DTOFranchisePOST dto)
        {
            Franchise franchise = _mapper.Map<Franchise>(dto);
            await _franchiseService.AddAsync(franchise);
            return CreatedAtAction("GetFranchiseAsync", new { id = franchise.Id }, franchise);
        }

        /// <summary>
        /// Removes a franchise from the database based on given id
        /// </summary>
        /// <param name="id">The ID of the franchise to delete.</param>
        /// <returns>An IActionResult indicating the status of the operation</returns>
        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchiseAsync(int id)
        {
            var franchise = await _franchiseService.GetByIdAsync(id);
            if(franchise == null)
            {
                return NotFound();
            }

            try
            {
                await _franchiseService.DeleteByIdAsync(id);
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }
        /// <summary>
        /// Controller method that updates the franchise ID for a specified array of movies.
        /// </summary>
        /// <param name="movieIds">An array of integers representing the IDs of the movies to update.</param>
        /// <param name="id">An integer representing the ID of the franchise to associate the movies with.</param>
        /// <returns>A task representing the asynchronous operation. If the update is successful, returns a NoContentResult. If the update fails, returns a NotFoundResult.</returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateMoviesForFranchiseAsync(int[] movieIds, int id)
        {
            try
            {
                await _franchiseService.UpdateMoviesAsync(movieIds, id);
                return NoContent();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
        }
        /// <summary>
        /// Controller method that retrieves a collection of characters associated with a specified franchise ID.
        /// </summary>
        /// <param name="id">An integer representing the ID of the franchise to retrieve characters for.</param>
        /// <returns>A task representing the asynchronous operation. If the operation is successful, returns an ActionResult containing a collection of DTOCharacterINFO objects. If the operation fails, returns a NotFoundResult.</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<DTOCharacterINFO>>> GetCharactersInFranchiseAsync(int id)
        {
            try
            {
                return Ok(_mapper.Map<List<DTOCharacterINFO>>
                    (await _franchiseService.GetCharactersInFranchiseAsync(id)));
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Controller method that retrieves a collection of movies associated with a specified franchise ID.
        /// </summary>
        /// <param name="id">An integer representing the ID of the franchise to retrieve movies for.</param>
        /// <returns>A task representing the asynchronous operation. If the operation is successful, returns an ActionResult containing a collection of DTOMovieINFO objects. If the operation fails, returns a NotFoundResult.</returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<DTOMovieINFO>>> GetMoviesInFranchiseAsync(int id)
        {
            try
            {
                return Ok(
                        _mapper.Map<List<DTOMovieINFO>>(
                            await _franchiseService.GetMoviesInFranchiseAsync(id)
                        )
                    );
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
        }
    }
}
