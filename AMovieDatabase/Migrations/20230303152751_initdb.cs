﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace AMovieDatabase.Migrations
{
    /// <inheritdoc />
    public partial class initdb : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Character",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    PictureUrl = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Character", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchise",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchise", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movie",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    ReleaseYear = table.Column<int>(type: "int", maxLength: 20, nullable: false),
                    Director = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    PosterUrl = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    TrailerUrl = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movie_Franchise_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchise",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CharacterMovie",
                columns: table => new
                {
                    CharacterId = table.Column<int>(type: "int", nullable: false),
                    MovieId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterMovie", x => new { x.CharacterId, x.MovieId });
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Character_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Character",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Movie_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "Id", "Alias", "Gender", "Name", "PictureUrl" },
                values: new object[,]
                {
                    { 1, "Iron Man", "Male", "Tony Stark", "https://example.com/ironman.jpg" },
                    { 2, null, "Male", "Frodo Baggins", "https://example.com/frodo.jpg" },
                    { 3, null, "Male", "Batman", "https://example.com/batman.jpg" },
                    { 4, null, "Male", "Alan Grant", "https://example.com/alangrant.jpg" },
                    { 5, null, "Male", "Luke Skywalker", "https://example.com/lukeskywalker.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "A movie about heroes", "Marvel Cinematic Universe" },
                    { 2, "A movie about heroes", "The Lord of the Rings" },
                    { 3, "A movie about heroes", "Batman" },
                    { 4, "A movie about heroes", "Jurassic Park" },
                    { 5, "A movie about heroes", "Star Wars" }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "PosterUrl", "ReleaseYear", "Title", "TrailerUrl" },
                values: new object[,]
                {
                    { 1, "Jon Favreau", 1, "Action, Adventure, Sci-Fi", "https://www.example.com/iron_man_poster.jpg", 2008, "Iron Man", "https://www.example.com/iron_man_trailer.mp4" },
                    { 2, "Peter Jackson", 2, "Action, Adventure, Drama", "https://www.example.com/lotr_poster.jpg", 2001, "The Lord of the Rings: The Fellowship of the Ring", "https://www.example.com/lotr_trailer.mp4" },
                    { 3, "Christopher Nolan", 3, "Action, Crime, Drama", "https://www.example.com/dark_knight_poster.jpg", 2008, "The Dark Knight", "https://www.example.com/dark_knight_trailer.mp4" },
                    { 4, "Steven Spielberg", 4, "Action, Adventure, Sci-Fi", "https://www.example.com/jurassic_park_poster.jpg", 1993, "Jurassic Park", "https://www.example.com/jurassic_park_trailer.mp4" },
                    { 5, "George Lucas", 5, "Action, Adventure, Fantasy", "https://www.example.com/star_wars_poster.jpg", 1977, "Star Wars: Episode IV - A New Hope", "https://www.example.com/star_wars_trailer.mp4" }
                });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 2, 2 },
                    { 3, 3 },
                    { 4, 4 },
                    { 5, 5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_MovieId",
                table: "CharacterMovie",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Movie_FranchiseId",
                table: "Movie",
                column: "FranchiseId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacterMovie");

            migrationBuilder.DropTable(
                name: "Character");

            migrationBuilder.DropTable(
                name: "Movie");

            migrationBuilder.DropTable(
                name: "Franchise");
        }
    }
}
