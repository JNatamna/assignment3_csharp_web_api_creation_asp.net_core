﻿using AMovieDatabase.Models.DTO.Franchises;
using AutoMapper;
using AMovieDatabase.Models.Entities;

namespace AMovieDatabase.Profiles
{
    public class FranchiseProfile : Profile
    {
        // This code is defining a mapping profile for a Franchise entity and its related DTOs
        public FranchiseProfile()
        {
            CreateMap<Franchise, DTOFranchise>()
                .ForMember(dto => dto.Movies, opt => opt
                .MapFrom(m => m.Movies.Select(c => c.Id).ToList()));

            CreateMap<DTOFranchisePOST, Franchise>();
            CreateMap<DTOFranchisePUT, Franchise>();
            CreateMap<Franchise, DTOFranchiseINFO>();
        }
    }
}
