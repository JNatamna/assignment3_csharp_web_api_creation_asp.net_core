﻿using AMovieDatabase.Models.DTO;
using AMovieDatabase.Models.DTO.Characters;
using AMovieDatabase.Models.Entities;
using AutoMapper;

namespace AMovieDatabase.Profiles
{
    public class CharacterProfile : Profile
    {
        // This code is defining a mapping profile for a character entity and its related DTOs
        public CharacterProfile() 
        {
            CreateMap<Character, DTOCharacter>()
                .ForMember(dto => dto.Movies, opt => opt
                .MapFrom(m => m.Movies.Select(c => c.Id).ToList()));

            CreateMap<DTOCharacterPOST, Character>();
            CreateMap<DTOCharacterPUT, Character>();
            CreateMap<Character, DTOCharacterINFO>();
        }
    }
}
