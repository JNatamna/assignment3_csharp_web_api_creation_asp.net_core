﻿using AMovieDatabase.Models.DTO.Movie;
using AMovieDatabase.Models.Entities;
using AutoMapper;

namespace AMovieDatabase.Profiles
{
    public class MovieProfile : Profile
    {
        // This code is defining a mapping profile for a Movie entity and its related DTOs
        public MovieProfile()
        {
            CreateMap<Movie, DTOMovie>().ForMember(dto => dto.Characters,
            opt => opt.MapFrom(m => m.Characters.Select(c => c.Id)))
            .ForMember(
            dto => dto.FranchiseId,
            opt => opt.MapFrom(m => m.Franchise.Id)
            );
            CreateMap<DTOMoviePOST, Movie>();
            CreateMap<DTOMoviePUT, Movie>();
            CreateMap<Movie, DTOMovieINFO>();
        }
    }
}
