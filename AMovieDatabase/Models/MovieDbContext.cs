﻿using AMovieDatabase.Models.Entities;
using Microsoft.EntityFrameworkCore;


namespace AMovieDatabase.Models
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Character> Characters { get; set; } = null!;
        public DbSet<Franchise> Franchises { get; set; } = null!;
        public DbSet<Movie> Movies { get; set; } = null!;

        // Seeds dummy data on creation of database
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Movies
            modelBuilder.Entity<Movie>().HasData(
                new Movie() { Id = 1, Title = "Iron Man", ReleaseYear = 2008, Director = "Jon Favreau", Genre = "Action, Adventure, Sci-Fi", PosterUrl = "https://www.example.com/iron_man_poster.jpg", TrailerUrl = "https://www.example.com/iron_man_trailer.mp4", FranchiseId = 1 },
                new Movie() { Id = 2, Title = "The Lord of the Rings: The Fellowship of the Ring", ReleaseYear = 2001, Director = "Peter Jackson", Genre = "Action, Adventure, Drama", PosterUrl = "https://www.example.com/lotr_poster.jpg", TrailerUrl = "https://www.example.com/lotr_trailer.mp4", FranchiseId = 2 },
                new Movie() { Id = 3, Title = "The Dark Knight", ReleaseYear = 2008, Director = "Christopher Nolan", Genre = "Action, Crime, Drama", PosterUrl = "https://www.example.com/dark_knight_poster.jpg", TrailerUrl = "https://www.example.com/dark_knight_trailer.mp4", FranchiseId = 3 },
                new Movie() { Id = 4, Title = "Jurassic Park", ReleaseYear = 1993, Director = "Steven Spielberg", Genre = "Action, Adventure, Sci-Fi", PosterUrl = "https://www.example.com/jurassic_park_poster.jpg", TrailerUrl = "https://www.example.com/jurassic_park_trailer.mp4", FranchiseId = 4 },
                new Movie() { Id = 5, Title = "Star Wars: Episode IV - A New Hope", ReleaseYear = 1977, Director = "George Lucas", Genre = "Action, Adventure, Fantasy", PosterUrl = "https://www.example.com/star_wars_poster.jpg", TrailerUrl = "https://www.example.com/star_wars_trailer.mp4", FranchiseId = 5 }
            );

            // Characters

            modelBuilder.Entity<Character>().HasData(
                new Character() { Id = 1, Name = "Tony Stark", Alias = "Iron Man", Gender = "Male", PictureUrl = "https://example.com/ironman.jpg" },
                new Character() { Id = 2, Name = "Frodo Baggins", Alias = null, Gender = "Male", PictureUrl = "https://example.com/frodo.jpg" },
                new Character() { Id = 3, Name = "Batman", Alias = null, Gender = "Male", PictureUrl = "https://example.com/batman.jpg" },
                new Character() { Id = 4, Name = "Alan Grant", Alias = null, Gender = "Male", PictureUrl = "https://example.com/alangrant.jpg" },
                new Character() { Id = 5, Name = "Luke Skywalker", Alias = null, Gender = "Male", PictureUrl = "https://example.com/lukeskywalker.jpg" }
            );

            // Franchises

            modelBuilder.Entity<Franchise>().HasData(
                new Franchise() { Id = 1, Name = "Marvel Cinematic Universe", Description = "A movie about heroes" },
                new Franchise() { Id = 2, Name = "The Lord of the Rings", Description = "A movie about heroes" },
                new Franchise() { Id = 3, Name = "Batman", Description = "A movie about heroes" },
                new Franchise() { Id = 4, Name = "Jurassic Park", Description = "A movie about heroes" },
                new Franchise() { Id = 5, Name = "Star Wars", Description = "A movie about heroes" }
            );
            //Configure many-to-many relationship between movies and characters.
            modelBuilder.Entity<Movie>()
                       .HasMany(m => m.Characters)
                       .WithMany(c => c.Movies)
                       .UsingEntity<Dictionary<string, object>>(
                           "CharacterMovie",
                           j => j.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                           j => j.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                           j =>
                           {
                               j.HasKey("CharacterId", "MovieId");
                               j.HasData(
                                   new { CharacterId = 1, MovieId = 1 },
                                   new { CharacterId = 2, MovieId = 1},
                                   new { CharacterId = 2, MovieId = 2 },
                                   new { CharacterId = 3, MovieId = 3 },
                                   new { CharacterId = 4, MovieId = 4 },
                                   new { CharacterId = 5, MovieId = 5 }
                               );
                           }
                       );
        }
    }
}


  
