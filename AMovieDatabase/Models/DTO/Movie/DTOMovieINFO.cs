﻿using System.ComponentModel.DataAnnotations;

namespace AMovieDatabase.Models.DTO.Movie
{
    public class DTOMovieINFO
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Title { get; set; } = null!;
    }
}
