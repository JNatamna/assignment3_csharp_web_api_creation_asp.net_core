﻿using System.ComponentModel.DataAnnotations;

namespace AMovieDatabase.Models.DTO.Movie
{
    public class DTOMovie
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Title { get; set; } = null!;
        [MaxLength(20)]
        public string Genre { get; set; } = null!;
        [MaxLength(20)]
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; } = null!;
        [MaxLength(200)]
        public string? PosterUrl { get; set; }
        [MaxLength(200)]
        public string? TrailerUrl { get; set; }
        public int? FranchiseId { get; set; }
        public List<int>? Characters { get; set; }
    }
}
