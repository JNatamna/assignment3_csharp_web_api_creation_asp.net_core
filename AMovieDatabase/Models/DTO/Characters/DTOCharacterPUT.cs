﻿using System.ComponentModel.DataAnnotations;

namespace AMovieDatabase.Models.DTO.Characters
{
    public class DTOCharacterPUT
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; } = null!;
        [MaxLength(50)]
        public string? Alias { get; set; }
        [MaxLength(20)]
        public string? Gender { get; set; }
        [MaxLength(200)]
        public string? PictureUrl { get; set; }
    }
}
