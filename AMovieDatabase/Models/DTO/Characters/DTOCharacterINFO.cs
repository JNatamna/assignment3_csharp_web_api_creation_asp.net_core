﻿using AMovieDatabase.Models.Entities;
using System.ComponentModel.DataAnnotations;

namespace AMovieDatabase.Models.DTO.Characters
{
    public class DTOCharacterINFO
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; } = null!;
    }
}
