﻿using AMovieDatabase.Models.Entities;
using System.ComponentModel.DataAnnotations;

namespace AMovieDatabase.Models.DTO.Characters
{
    public class DTOCharacter
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; } = null!;
        [MaxLength(50)]
        public string Alias { get; set; } = null!;
        [MaxLength(20)]
        public string Gender { get; set; } = null!;
        [MaxLength(200)]
        public string PictureUrl { get; set; } = null!;
        public List<int> Movies { get; set; } = null!;
    }
}
