﻿using System.ComponentModel.DataAnnotations;

namespace AMovieDatabase.Models.DTO.Franchises
{
    public class DTOFranchise
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; } = null!;
        [MaxLength(200)]
        public string Description { get; set; } = null!;
        public List<int>? Movies { get; set; }
    }
}
