﻿using System.ComponentModel.DataAnnotations;

namespace AMovieDatabase.Models.DTO.Franchises
{
    public class DTOFranchisePOST
    {
        [MaxLength(50)]
        public string Name { get; set; } = null!;
        [MaxLength(200)]
        public string Description { get; set; } = null!;
    }
}
