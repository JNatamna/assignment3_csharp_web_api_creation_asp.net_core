﻿using System.ComponentModel.DataAnnotations;

namespace AMovieDatabase.Models.DTO.Franchises
{
    public class DTOFranchiseINFO
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; } = null!;
    }
}
