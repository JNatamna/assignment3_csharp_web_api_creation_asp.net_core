﻿using AMovieDatabase.Models.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AMovieDatabase.Models.Entities
{
    [Table("Franchise")]
    public class Franchise
    {
        public Franchise()
        {
            Movies = new HashSet<Movie>();
        }

        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; } = null!;
        [MaxLength(200)]
        public string Description { get; set; } = null!;

       // Relations
        public ICollection<Movie> Movies { get; set; }
    }
}
