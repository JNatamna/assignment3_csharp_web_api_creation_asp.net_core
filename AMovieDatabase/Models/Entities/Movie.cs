﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AMovieDatabase.Models.Entities
{
    [Table("Movie")]
    public class Movie
    {
        public Movie()
        {
            Characters = new HashSet<Character>();
        }

        public int Id { get; set; }
        [MaxLength(50)]
        public string Title { get; set; } = null!;
        [MaxLength(50)]
        public string Genre { get; set; } = null!;
        [MaxLength(20)]
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; } = null!;
        [MaxLength(200)]
        public string? PosterUrl { get; set; }
        [MaxLength(200)]
        public string? TrailerUrl { get; set; }

        // Relations
        public int? FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }
        public ICollection<Character> Characters { get; set; }
    }
}
