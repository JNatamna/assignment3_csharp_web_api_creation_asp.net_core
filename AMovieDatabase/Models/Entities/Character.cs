﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AMovieDatabase.Models.Entities
{
    [Table("Character")]
    public partial class Character
    {
        public Character()
        {
            Movies = new List<Movie>();
        }

        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; } = null!;
        [MaxLength(50)]
        public string? Alias { get; set; }
        [MaxLength(10)]
        public string? Gender { get; set; }
        [MaxLength(200)]
        public string? PictureUrl { get; set; }

        // Relations
        public ICollection<Movie> Movies { get; set; }

    }
}

